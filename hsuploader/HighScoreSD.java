package hsuploader;

import java.io.*;
import java.nio.file.FileSystem;

public class HighScoreSD {
	
	private String hs = "";
	
	public HighScoreSD(){
		
	    System.out.println("Starten met het lezen van de SD kaart");
		
		File highscores = new File("G:\\SCORE.HSC");
		
		try {
		    BufferedReader br = new BufferedReader(new FileReader(highscores));
		    
			int counter = 0;
			String currname = "";
			long currScore = 0;
			long currentchar = 0;
			while ((currentchar = br.read()) != -1) //read all bytes from file.
			{

				if (counter <= 2) //getting next name (bytes 0-2)
					currname += (char) currentchar;

				if (counter > 2) //getting next score (bytes 3-6)
				{
					currScore |= ((long) currentchar) << (24 - ((counter-3)*8));
				}
				if (counter == 6) // finalize current score and reset counter
				{

					this.hs += currname + "," + Long.toString(currScore) + ";";

					currname = "";
					counter = 0;
				} else
					counter++; //read next item
			}
		    br.close();
		    System.out.println("Het lezen van de SD kaart is gelukt");
		}
		catch (IOException e) {
		    System.out.println("Het lezen van de SD kaart is mislukt");
		}
		
	}
	
	public String getHS(){
		return this.hs;
	}
}

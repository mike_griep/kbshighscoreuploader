package hsuploader;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class DisplayHSscreen extends JFrame implements ActionListener{

	private JMenuBar mbMenuBar;
	private JMenu menuGetHS;
	private JMenuItem mnuGetHSSe, mnuGetHSSD;
	private JLabel lbTitle;
	private JSeparator sep;
	private HighScoreSD display;
	private JPanel panel;
	private HighScoreSe getSe;
	private JTextArea taHS;
	private String hss = "";
	
	public DisplayHSscreen(){
		
		super();
		getSe = new HighScoreSe(this);
		this.panel = new JPanel();
		
		this.setBackground(Color.gray);
		this.setSize(300,400);
		this.setTitle("Highscores");	

		
		this.mbMenuBar = new JMenuBar();
		this.menuGetHS = new JMenu("Highscores");
		this.mbMenuBar.add(menuGetHS);
		this.mnuGetHSSe = new JMenuItem("Highscore ophalen via serial");
		this.menuGetHS.add(mnuGetHSSe);
		this.mnuGetHSSD = new JMenuItem("Highscore ophalen via SD");
		//this.menuGetHS.add(mnuGetHSSD);
		
		this.setJMenuBar(mbMenuBar);
		

		
		this.lbTitle = new JLabel();
		this.lbTitle.setText("Highscores");
		this.lbTitle.setAlignmentX(CENTER_ALIGNMENT);	
		this.taHS = new JTextArea();
		this.taHS.setEditable(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel.setLayout(new BorderLayout());
		panel.add(this.lbTitle,BorderLayout.NORTH);
		panel.add(this.taHS,BorderLayout.CENTER);

		this.add(panel);

		this.setVisible(true);
		
		this.mnuGetHSSe.addActionListener(this);
		this.mnuGetHSSD.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == mnuGetHSSe) {
			this.getSe.start();
		}else if(e.getSource() == mnuGetHSSD) {	
			HighScoreSD hsSD = new HighScoreSD();
			this.decodeOutput(hsSD.getHS());
		}else if(e.getSource() == getSe){
			this.decodeOutput(this.getSe.getHS());
		}
	}
	
	//decode the output of the serial
	private void decodeOutput(String hs){
		String[] highscores = hs.split(";");
		for (String sEntry : highscores) {
			String[] entry = sEntry.split(",");
			
			this.hss += entry[0] + "\t";
			this.hss += entry[1] + "\n";
			
		}
		this.taHS.setText(hss);
		this.revalidate();
	}
	
	

}
